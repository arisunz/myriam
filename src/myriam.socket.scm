(module (myriam socket)

    (make-server-socket!
     make-client-socket!
     make-heartbeat-listener
     send-heartbeat-and-die!
     wait-for-heartbeat-and-die!)

  (import scheme
          (chicken base)
          (myriam address)
          (myriam messaging)
          nng)

  (define (make-server-socket! addr)
    (let ((socket (make-rep-socket)))
      (nng-listen socket (address->binding addr))
      socket))

  (define (make-client-socket! addr)
    (let ((socket (make-req-socket)))
      (nng-dial socket addr)
      socket))

  (define (make-heartbeat-listener addr)
    (let ((socket (make-pair-socket)))
      (nng-listen socket addr)
      socket))

  (define (send-heartbeat-and-die! addr)
    (let ((socket (make-pair-socket)))
      (nng-dial socket addr)
      (nng-send socket "alive")))

  (define (wait-for-heartbeat-and-die! socket)
    (nng-recv socket)))
