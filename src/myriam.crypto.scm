(module (myriam crypto)

    ;;;; Authentication helpers

    (fetch-public-key!
     fetch-secret-key!
     fetch-keypair!
     hash-blob
     encrypt
     decrypt
     make-nonce!)

  (import scheme
          (chicken base)
          (chicken condition)
          (chicken file)
          (chicken format)
          (chicken platform)
          (chicken random)
          message-digest-byte-vector
          (myriam utils)
          (only srfi-1 iota)
          sha2
          srfi-4
          srfi-18
          tweetnacl)

  (define secret-key-name (make-parameter "secret.key"))
  (define public-key-name (make-parameter "public.key"))

  (define (key-path key-name)
    (string-append (myriam-directory) key-name))

  (define (fetch-public-key!)
    (fetch-key! (public-key-name)))

  (define (fetch-secret-key!)
    (fetch-key! (secret-key-name)))

  (define (fetch-key! key-name)
    (call/cc
     (lambda (k)
       (condition-case (with-input-from-file
                           (key-path key-name)
                         (lambda ()
                           (read)))
         ((exn file) (begin
                       (generate-keypair!)
                       (k (fetch-key! key-name))))
         (exn () (signal exn))))))

  (define (fetch-keypair!)
    (values
     (fetch-public-key!)
     (fetch-secret-key!)))

  (define (generate-keypair!)
    (let-values (((pk sk) (make-asymmetric-box-keypair)))
      (for-each
       (lambda (k n)
         (with-output-to-file (key-path n)
           (lambda () (write k))))
       (list pk sk)
       (list (public-key-name) (secret-key-name)))))

  (define (hash-blob blob)
    (message-digest-blob (sha256-primitive) blob))

  (define (make-first-nonce!)
    (set-pseudo-random-seed! (random-bytes))
    (apply
     u8vector
     (map (lambda (x)
            (pseudo-random-integer 256))
          (iota symmetric-box-noncebytes))))

  (define make-nonce!
    (let ((last-nonce (make-first-nonce!))
          (nonce-mutex (make-mutex)))
      (lambda ()
        (with-lock
         nonce-mutex
         (let ((new-nonce (increment-u8vector last-nonce)))
           (set! last-nonce new-nonce)
           (mutex-unlock! nonce-mutex)
           new-nonce)))))

  (define (increment-u8vector u8vec)
    ;; we treat the vector as if its leftmost element is the least significant
    ;; functionally the same for our intents and purposes, and there's no need to double reverse
    (letrec ((carries? (lambda (x)
                         (> (add1 x) 255)))
             (increment (lambda (inc xs)
                          (cond ((null? xs) inc)
                                ((not (carries? (car xs)))
                                 (append (reverse inc) (cons (add1 (car xs)) (cdr xs))))
                                (else (increment (cons 0 inc)
                                                 (cdr xs)))))))
      (let ((u8lst (u8vector->list u8vec)))
        (list->u8vector (increment '() u8lst)))))

  (define (encrypt msg pk sk nonce)
    ((asymmetric-box pk sk) msg nonce))

  (define (decrypt cypher pk sk nonce)
    (let ((msg ((asymmetric-unbox pk sk) cypher nonce)))
      (if (not msg)
          (signal (condition '(exn type crypt message "failed to decrypt incoming message")))
          msg))))
