(module (myriam utils)

    (myriam-directory
     with-timeout
     with-lock)

  (import scheme
          (chicken base)
          (chicken condition)
          (chicken file)
          (chicken platform)
          forcible
          srfi-18)

  (define myriam-directory
    (let ((path (string-append
                 (system-config-directory)
                 "/chicken/myriam/")))
      (lambda ()
        (unless (directory-exists? path)
          (create-directory path #t))
        path)))

  (define-syntax with-timeout
    (syntax-rules ()
      ((with-timeout n body ...)
       (force (future/timeout
               n (begin body ...))
              (lambda (x)
                (if (timeout-condition? x)
                    #f
                    (signal x)))))))

  (define-syntax with-lock
    (syntax-rules ()
      ((_ lock body ...)
       (dynamic-wind
         (lambda () (mutex-lock! lock))
         (lambda () body ...)
         (lambda () (mutex-unlock! lock)))))))
