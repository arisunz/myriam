(module (myriam messaging)

    (msg message?
         message-head
         message-body
         message-async?
         set-message-async!
         receive-message
         receive-message/retry
         send-message)

  (import scheme
          (chicken base)
          (chicken condition)
          (chicken foreign)
          (chicken platform)
          (chicken port)
          base64
          medea
          protobuf
          protobuf-generic
          srfi-4
          (myriam crypto)
          (myriam identity)
          nng)

  (define-record-type message
    (make-message head body async?)
    message?
    (head message-head)
    (body message-body)
    (async? message-async? set-message-async!))

  (define (msg head . body)
    (make-message
     head
     body
     #t))

  (define (receive-message socket)
    (unwrap
     (nng-recv socket)))

  (define (receive-message/retry socket)
    (with-exception-handler
        (lambda (x)
          (print-error-message x)
          (receive-message/retry socket))
      (lambda ()
        (receive-message socket))))

  (define (send-message socket msg)
    (nng-send
     socket
     (wrap msg)))

  (define (wrap msg)
    (let ((nonce (make-nonce!)))
      (json->string
       `((nonce . ,(list->vector (u8vector->list nonce)))
         (sender . ,(self-identity-hash (current-self-identity)))
         (cypher . ,(base64-encode
                     (encrypt
                      (serialize-message msg)
                      (public-identity-key (current-target-identity))
                      (self-identity-secret-key (current-self-identity))
                      nonce)))))))

  (define (unwrap data)
    (let* ((box (read-json data))
           (public-key (public-identity-key (fetch-identity/hash (alist-ref 'sender box)))))
      (if public-key
          (deserialize-message
           (decrypt
            (base64-decode
             (alist-ref 'cypher box))
            public-key
            (self-identity-secret-key (current-self-identity))
            (list->u8vector
             (vector->list
              (alist-ref 'nonce box)))))
          (signal (condition '(exn type crypt message "incoming message sender not in trust store"))))))

  (define (serialize-message msg)
    (with-output-to-string
       (lambda ()
         (serialize msg))))

  (define (deserialize-message data)
    (with-input-from-string data
      (lambda ()
        (deserialize)))))
