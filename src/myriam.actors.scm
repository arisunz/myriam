(module (myriam actors)
    (spawn send send* msg
           self storage message-type
           spec spec?
           send-timeout
           current-self-identity
           make-public-identity
           store-identity!)
  (import scheme
          (chicken base)
          (chicken condition)
          srfi-18
          srfi-69
          (myriam actors spec)
          (myriam address)
          (myriam identity)
          (myriam messaging)
          (myriam socket)
          (myriam utils))

  (define self (make-parameter #f))
  (define tasks (make-parameter '()))
  (define storage (make-parameter '()))
  (define send-timeout (make-parameter 2))
  (define listener (make-parameter #f))
  (define message-type (make-parameter #f))
  (current-self-identity (make-self-identity!))

  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;;; Storage API
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  (define (store-value! k v)
    (hash-table-set! (storage) k v))

  (define (fetch-value k)
    (hash-table-ref/default (storage) k #f))

  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;;; Message handling
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  (define (handle-message! msg)
    (let* ((task (hash-table-ref/default (tasks) (message-head msg) #f))
           (sync? (not (message-async? msg)))
           (default (lambda () 'OK)))
      (case (message-head msg)
        ((stop!) (values default #f))
        ((ping) (values (lambda () 'pong) #t))
        ((store!)
         (values (wrap-task (lambda () (apply store-value! (message-body msg))) #t) #t))
        ((fetch)
         (values (wrap-task (lambda () (apply fetch-value (message-body msg))) #t) #t))
        (else
         (values
          (if task
              (wrap-task (lambda () (apply task (message-body msg))) sync?)
              (lambda () 'NOT-FOUND))
          #t)))))

  (define (wrap-task proc sync?)
    (if sync?
        (lambda () (safe-call proc))
        (lambda () (thread-start! proc) 'OK)))

  (define (safe-call proc)
    (call/cc
     (lambda (k)
       (with-exception-handler
           (lambda (x) (k x))
         (lambda () (k (proc)))))))

  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;;; Main sauce
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  (define (spawn #!optional (spec '()) (port #f))
    (if (or (null? spec) (spec? spec))
        (parameterize ((self (if port (make-address #:port port) (make-address)))
                       (tasks (alist->hash-table spec))
                       (storage (make-hash-table))
                       (listener (make-local-address))
                       (current-self-identity (make-self-identity!))
                       (current-target-identity (self->public-identity (current-self-identity))))
          (store-identity! (self->public-identity (current-self-identity)) (self))
          (let* ((listener-socket (make-heartbeat-listener (listener)))
                 (thread (thread-start!
                          (lambda ()
                            (let ((socket (make-server-socket! (self))))
                              (send-heartbeat-and-die! (listener))
                              (let loop ((env (receive-message/retry socket)))
                                (parameterize ((message-type (if (message-async? env) 'async 'sync)))
                                  (let-values (((task continue?) (handle-message! env)))
                                    (send-message socket (task))
                                    (when continue?
                                      (loop (receive-message/retry socket)))))))))))
            (if (with-timeout 2 (wait-for-heartbeat-and-die! listener-socket))
                (values (self) thread)
                (begin
                  (thread-terminate! thread)
                  (signal (condition '(exn type actor message "failed to spawn actor: timeout")))))))
        (signal (condition '(exn type spec message "not an actor spec")))))

  (define (post address msg)
    (let ((target-identity (fetch-identity/address address)))
      (if target-identity
          (parameterize ((current-target-identity target-identity))
            (with-timeout
             (send-timeout)
             (let ((socket (make-client-socket! address)))
               (send-message socket msg)
               (receive-message socket))))
          (signal (condition '(exn type crypt message "target actor not in trust store"))))))

  (define (send address msg)
    (if (message? msg)
        (let ((result (post address msg)))
          (if (condition? result)
              (signal result)
              result))
        (signal (condition '(exn type message message "invalid type passed to send")))))

  (define (send* address msg)
    (set-message-async! msg #f)
    (if (message? msg)
        (let ((result (post address msg)))
          (if (condition? result)
              (signal result)
              result))
        (signal (condition '(exn type message message "invalid type passed to send*"))))))
