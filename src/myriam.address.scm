(module (myriam address)
    (make-address
     make-local-address
     address->binding
     change-host
     address-port
     address-host)
  (import scheme
          (chicken base)
          (chicken format)
          (chicken string)
          (chicken tcp)
          uuid)

  (define (unused-port)
    (let* ((listener (tcp-listen 0))
           (port (tcp-listener-port listener)))
      (tcp-close listener)
      port))

  (define (make-address #!key (host "127.0.0.1") (port (unused-port)))
    (format "tcp://~a:~a"
            host
            port))

  (define (make-local-address)
    (format "inproc://~a"
            (uuid)))

  (define (make-binding-address)
    (make-address "*"))

  (define (address-tokens addr)
    (string-split (substring addr 6) ":"))

  (define (address-host addr)
    (car (address-tokens addr)))

  (define (address-port addr)
    (cadr (address-tokens addr)))

  (define (change-host addr host)
    (format "tcp://~a:~a"
            host
            (address-port addr)))

  (define (address->binding addr)
    (change-host addr "*")))
