(module (myriam actors spec)
    (spec spec?)
  (import scheme
          (chicken base)
          (chicken condition))

  (define (andmap f xs)
    (if (null? xs)
        #t
        (call/cc
         (lambda (return)
           (let loop ((x (car xs)) (xs (cdr xs)))
             (cond ((and (f x) (null? xs)) (return #t))
                   ((not (f x)) (return #f))
                   (else (loop (car xs) (cdr xs)))))))))

  (define (spec? x)
    (andmap (lambda (p)
              (and (pair? p)
                   (symbol? (car p))
                   (procedure? (cdr p))))
            x))

  (define-syntax spec
    (syntax-rules (->)
      ((spec (name -> action) ...)
       (let ((s (list (cons 'name action) ...)))
         (if (not (spec? s))
             (signal (condition '(exn type spec message "invalid spec")))
             s))))))
