SRC=src/*.scm

build: $(SRC)
	chicken-install -n

test: build
	csi tests/run.scm

clean-test: clean build test

clean-build: clean build

clean:
	chicken-clean
