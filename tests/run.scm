(import test
        (chicken blob)
        (myriam actors)
        (myriam address)
        (myriam identity)
        (only miscmacros begin0))

(define actor (spawn (spec (foo -> (lambda (x y) (expt x y))))))

(test-group "actors"
            (test "Async message"
                  'OK (send actor (msg 'foo 5 7)))

            (test "Sync message"
                  78125 (send* actor (msg 'foo 5 7)))

            (test "Ping actor"
                  'pong (send* actor (msg 'ping)))

            (test "Store and fetch value"
                  "banana"
                  (begin
                    (send actor (msg 'store! 'important-message "banana"))
                    (send* actor (msg 'fetch 'important-message))))

            (test "Message types"
                  '(async sync)
                  (let* ((register (spawn))
                         (teller (spawn (spec (async -> (lambda ()
                                                          (send register
                                                                (msg 'store! 'async (message-type)))))
                                              (sync -> (lambda ()
                                                         (send register
                                                               (msg 'store! 'sync (message-type)))))))))
                    (send teller (msg 'async))
                    (send* teller (msg 'sync))
                    (begin0
                     (list (send* register (msg 'fetch 'async)) (send* register (msg 'fetch 'sync)))
                     (send teller (msg 'stop!))
                     (send register (msg 'stop!)))))

            (test "Specify port"
                  "1337"
                  (begin
                    (let ((a (spawn (spec) 1337)))
                      (begin0
                       (address-port a)
                       (send a (msg 'stop!))))))

            (test-error "Invalid call"
                        (send* actor (msg 'foo 7)))

            (test-error "Not a message"
                        (send actor 'foo))

            (test "Kill actor"
                  'OK (send actor (msg 'stop!)))

            (test-assert "Spawn a bunch of actors"
              (let ((a (spawn))
                    (b (spawn))
                    (c (spawn))
                    (d (spawn))
                    (e (spawn))
                    (stuff (lambda (x m)
                             (send x (msg 'store! "stuff" m))))
                    (get-stuff (lambda (x)
                                 (send* x (msg 'fetch "stuff"))))
                    (stop (lambda (x)
                            (send x (msg 'stop!)))))
                (for-each stuff (list a b c d e) (list "a" "b" "c" "d" "e"))
                (begin0
                 (equal? (list "a" "b" "c" "d" "e") (map get-stuff (list a b c d e)))
                 (for-each stop (list a b c d e)))))

            #;(test "Unauthorized actor"
                  #f (let ((a (spawn (spec (foo -> (lambda () "bar"))))))
                       (parameterize ((key (string->blob "aathisaaaaaaisaaaaaaaaaaafakeaaa"))
                                      (send-timeout 1))
                         (send* a (msg 'foo))))))

(test-exit)
