(module (myriam identity)

    *

  (import scheme
          (chicken base)
          (chicken condition)
          (myriam crypto)
          (myriam utils)
          (srfi 18)
          (srfi 69))

  (define current-self-identity (make-parameter #f))
  (define current-target-identity (make-parameter #f))

  (define identity-registry (make-hash-table #:test string=?))
  (define address-registry (make-hash-table #:test string=?))
  (define registry-mutex (make-mutex))

  (define-record-type self-identity
    (mk-self-identity pk sk hash) self-identity?
    (pk self-identity-public-key)
    (sk self-identity-secret-key)
    (hash self-identity-hash))

  (define-record-type public-identity
    (mk-public-identity pk hash) public-identity?
    (pk public-identity-key)
    (hash public-identity-hash))

  (define (make-self-identity!)
    (let-values (((pk sk) (fetch-keypair!)))
      (mk-self-identity
       pk sk
       (hash-blob pk))))

  (define (make-public-identity pk)
    (mk-public-identity pk (hash-blob pk)))

  (define (self->public-identity self)
    (make-public-identity
     (self-identity-public-key self)))

  (define (fetch-identity/hash hash)
    (with-lock
     registry-mutex
     (hash-table-ref/default
      identity-registry
      hash
      #f)))

  (define (fetch-identity/address address)
    (with-lock
     registry-mutex
     (hash-table-ref/default
      address-registry
      address
      #f)))

  (define (store-identity! identity address)
    (cond ((public-identity? identity)
           (with-lock
            registry-mutex
            (hash-table-set!
             identity-registry
             (public-identity-hash identity)
             identity)
            (hash-table-set!
             address-registry
             address
             identity)))
          ((self-identity? identity)
           (store-identity! (self->public-identity identity) address))
          (else (signal (condition '(exn type identity message "wrong type passed to store-identity!")))))))
