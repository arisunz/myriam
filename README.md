# Myriam
Simple attempt at implementing the [actor model](https://en.wikipedia.org/wiki/Actor_model) for secure, distributed programming in CHICKEN Scheme. Feedback is very welcome!

The API is very much unstable at the moment.

# Requirements

Myriam stands on the shoulders of giants.

* `libnng`
* CHICKEN 5
* Eggs:
  * `base64`
  * `foreigners`
  * `medea`
  * `message-digest-utils`
  * `protobuf`
  * `sha2`
  * `srfi-1`
  * `srfi-18`
  * `srfi-69`
  * `tweetnacl`
  * `uuid`
  * `nng` (https://gitlab.com/ariSun/chicken-nng)

# Installation
Your typical `git clone` and `chicken-install` inside the cloned repo.

# Usage

The `(myriam actors)` module provides everything necessary to get you up and running.

This is just an overview. Take a look at the [tests](tests/run.scm) for more examples.

## spec
`[syntax] (spec (name-1 -> proc-1) (name-2 -> proc-2) ... (name-n -> proc-n)) -> spec?`

Create an actor specification which can be reused.

## spawn
`[procedure] (spawn #!optional [spec? [port]]) -> (values address thread)`

Spawn a fresh actor and return its address and thread.

## msg
`[procedure] (msg name . args) -> message?`

Create a message.

## send
`[procedure] (send address message)`

Send an asynchronous message to an actor. Returns `'OK` if the message was sent successfully, and raises an error if something other than a `message?` is passed.

## send*
`[procedure] (send* address message?)`

Send a synchronous message to an actor and get a result, or raised condition if there was an error. Note that the actor will not receive further messages until it's done processing a task invoked from a synchronous message.

## make-public-identity!
`[procedure] (make-public-identity public-key) -> public-identity?`

Create a public identity from a public key.

## store-identity!
`[procedure] (store-identity! public-identity address)`

Store a trusted public identity in the local registry.

## send-timeout
`[parameter] (send-timeout) -> int`

Time to wait for a reply until a timeout condition is raised.

## self

`[parameter] (self) -> address`

Inside an actor, refers to its own address. Useful when defining actor specs.

## message-type
`[parameter] (message-type) -> (or 'async 'sync)`

Inside an actor, tell whether the message being currently processed is async or sync. Useful for checks.

## current-self-identity
`[parameter] (current-self-identity) -> self-identity?`

The default, private identity corresponding to the host in which an actor spawns.

## Pre-defined Messages

### ping
`[message] (send address (msg 'ping))`

Check if a given actor is still alive.

### stop!
`[message] (send address (msg 'stop!))`

Stop an actor. Currently does not interrupt any running task.

### store!
`[message] (send address (msg 'store! key value))`

Store `value` with `key` inside the actor's local storage. Keep in mind that already-running tasks get a copy of the storage as soon as they are started and thus won't see any new changes. Also note that even if you use `send`, this message will be treated as synchronous.

### fetch
`[message] (send* address (msg 'fetch key))`

Fetch a value stored with `key` inside actor's local storage, or `#f` if no such value exists. Note that even if you use `send`, this message will be treated as synchronous.

## Authentication and Identities

One of Myriam's goals is to be suitable as a building block for building distributed, P2P systems. As such, messages between actors are secured with asymmetric encryption. Actors can only receive messages from (and send messages to) trusted *identities* stored in a local registry.

An identity maps to a host, and therefore multiple actors can and do share a single identity. There's two kinds of identities. Self-identities contain a secret key and should be kept, well, secret. Public identities are shared between trusted hosts.

Identity management is beyond Myriam's scope, but handy procedures and parameters are provided as starting points.

# Example

Dumb and simple example. Note that any of the actors could be running on a different machine.

```scheme
(import (myriam actors)
        srfi-69
        uuid)

(define logger
  (spawn (spec
          (write -> (lambda (msg #!optional (log-prefix "[LOG]"))
                     (with-output-to-file "log.txt"
                       (lambda ()
                         (import (chicken time posix)
                                 (chicken format))
                         (printf "~a [~a] ~a~n"
                                 log-prefix
                                 (seconds->string)
                                 msg))
                       #:append)))
          (log -> (lambda (m)
                   (send (self) (msg 'write m "[LOG]"))))

          (error -> (lambda (m)
                     (send (self) (msg 'write m "[ERROR]")))))))

;; define actor with no tasks, will only react to predefined special messages:
;; stop, ping, store and fetch
(define broker (spawn))

(define exponentiator
  (spawn (spec
          (run-async -> (lambda (x y)
                         (send logger
                           (msg 'log (expt x y)))

                         (send broker (msg 'store! (string->symbol (uuid)) (expt x y)))))

          (run-sync -> (lambda (x y)
                        (expt x y))))))

(define (stop-all!)
  (for-each
   (lambda (a)
     (send a (msg 'stop!)))
   (list logger broker exponentiator)))

(on-exit stop-all!)
```

# Notes
## Current limitations/caveats
* Synchronous messages block execution of an actor for the duration of the task.
* Internally, there is no difference between a sync and an async task. Care must be taken to send messages correctly for a given task. You want plain `send` when you don't care about the immediate result of a task, or when the task is arranged to send the result somewhere else, and you want `send*` when you do want such a result.
* No assumptions are made about key distribution and service (actor) discovery mechanisms.

# License
(C) 2021 - Ariela Wenner

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
